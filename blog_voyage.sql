-- Active: 1673947702029@@127.0.0.1@3306@blog_voyage
DROP TABLE IF EXISTS article_category;
DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS counter;


CREATE TABLE counter(  
    id int PRIMARY KEY AUTO_INCREMENT,
    number VARCHAR(255)
);
CREATE TABLE category(
    id int  PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (100)
);
CREATE TABLE article (
    id int PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR (255),
    content VARCHAR (5000),
    img VARCHAR (10000),
    date DATE,
    author VARCHAR(100),
    id_counter int,
    Foreign Key (id_counter) REFERENCES counter(id) ON DELETE CASCADE
);

CREATE TABLE `comments`(
    id int PRIMARY KEY AUTO_INCREMENT,
    content VARCHAR (500),
    id_article int,
    Foreign Key (id_article) REFERENCES article(id) ON DELETE CASCADE
);

CREATE TABLE article_category(
    id_category int,
    id_article int,
    PRIMARY KEY (id_category, id_article),
    Foreign Key (id_category) REFERENCES category(id) ON DELETE CASCADE,
    Foreign Key (id_article) REFERENCES article(id) ON DELETE CASCADE
);




INSERT INTO counter (number) VALUES 
(15), (25), (32), (12);

INSERT INTO category (name) VALUES 
('Meilleures capitales'),
('Plus beaux restaurants'),
('Europe'),
('Amériques');


INSERT INTO article (title, content, img, date, author, id_counter) VALUES
('Paris','Visitez la capitale de la mode','https://images.pexels.com/photos/5466977/pexels-photo-5466977.jpeg?auto=compress&cs=tinysrgb&w=600','2020-02-01','Marc Latour',1),
('Lisbonne','Découvrez la magnifique ville de Lisbonne','https://images.pexels.com/photos/13512718/pexels-photo-13512718.jpeg?auto=compress&cs=tinysrgb&w=600','2019-02-01','Joao Delgado',2),
('Santiago du Chili','Partez à l\'aventure Outre-Atlantique','https://images.pexels.com/photos/13437426/pexels-photo-13437426.jpeg?auto=compress&cs=tinysrgb&w=600','2018-03-01','Tom Pouce',3),
('The singular Santiago','Le meilleur de la région','https://images.pexels.com/photos/14035600/pexels-photo-14035600.jpeg?auto=compress&cs=tinysrgb&w=600','2018-03-10','Tom Pouce',3);

INSERT INTO comments (content, id_article) VALUES
('Très bel article', 1),
('Super photo!',2),
('Je réserve mon avion!', 3),
('Testé et approuvé.', 4);


INSERT INTO article_category(id_category, id_article) VALUES 
(1,3),(2,4),(3,2),(4,3);

SELECT * from counter;

SELECT * 
from article
left join article_category
on article.id=article_category.id_article;

SELECT comments.content
from comments
left join article a
on a.id=id_article
where id_article=2;

SELECT * from comments;
SELECT * from category;
