<?php

namespace App\Controller;
use App\Repository\ArticleRepository;
use App\Entities\Article;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\CommentsRepository;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


#[Route('/api/article')]
class ArticleController extends AbstractController
{
    private ArticleRepository $repo;
    private CommentsRepository $repoComment;

    public function __construct(ArticleRepository $repo, CommentsRepository $repoComment)
    {
        $this->repo = $repo;
        $this->repoComment = $repoComment;
    }

    #[Route(methods: 'GET')]
    public function all() {
        $articles = $this->repo->findAll();
        return $this->json($articles);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $article = $this->repo->findById($id);
        if(!$article){
            throw new NotFoundHttpException();
        }
        
        return $this->json($article);
 
    }


    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator) {
        try{
            $article = $serializer->deserialize($request->getContent(), Article::class, 'json');
            $this->repo->persist($article);
            return $this->json($article,  Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator) {   
        $article = $this->repo->findById($id);
        if(!$article){
            throw new NotFoundHttpException();
        }
        try{
            $toUpdate = $serializer->deserialize($request->getContent(), Article::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);
    
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
 
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $article = $this->repo->findById($id);
        if(!$article){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($article);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}/comments', methods: 'GET')]
    public function commentsByArticle(int $id) {
        $commentRepository = $this->repo->findById($id);
        // var_dump($comment);
        $commentRepository = $this->repoComment->findById($id);
        // var_dump($commentRepository);

        if(!$commentRepository){
            throw new NotFoundHttpException();
        }

        // $this->repo->findCommentsByArticle($commentRepository);
        return $this->json($commentRepository);
    }
}