<?php

namespace App\Controller;
use App\Repository\CommentsRepository;
use App\Entities\Comments;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

#[Route('/api/comments')]
class CommentsController extends AbstractController
{
    private CommentsRepository $repo;

    public function __construct(CommentsRepository $repo)
    {
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all() {
        $comments = $this->repo->findAll();
        return $this->json($comments);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $comments = $this->repo->findById($id);
        if(!$comments){
            throw new NotFoundHttpException();
        }
        return $this->json($comments);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        $comments = $serializer->deserialize($request->getContent(), Comments::class, 'json');
        $this->repo->persist($comments);
        return $this->json($comments,  Response::HTTP_CREATED);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $comments = $this->repo->findById($id);
        if(!$comments){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Comments::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
        
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $comments = $this->repo->findById($id);
        if(!$comments){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($comments);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }    


    

}