<?php

namespace App\Controller;
use App\Repository\CounterRepository;
use App\Entities\Counter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

#[Route('/api/counter')]
class CounterController extends AbstractController
{
    private CounterRepository $repo;

    public function __construct(CounterRepository $repo)
    {
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all() {
        $counter = $this->repo->findAll();
        return $this->json($counter);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $counter = $this->repo->findById($id);
        if(!$counter){
            throw new NotFoundHttpException();
        }
        return $this->json($counter);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        $counter = $serializer->deserialize($request->getContent(), Counter::class, 'json');
        $this->repo->persist($counter);
        return $this->json($counter,  Response::HTTP_CREATED);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $counter = $this->repo->findById($id);
        if(!$counter){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Counter::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
        
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $counter = $this->repo->findById($id);
        if(!$counter){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($counter);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }


}