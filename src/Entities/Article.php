<?php



namespace App\Entities;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

class Article {
    private ?int $id;
	#[Assert\NotBlank]
    private ?string $title;
	#[Assert\NotBlank(allowNull: true)]
    private ?string $content;
	
    private ?string $img;
	
    private ?string $author;
    private ?DateTime $date;
	

	public function __construct(?string $title, ?string $content, ?string $img, ?string $author, ?DateTime $date = null, ?int $id = null){
		$this->id = $id;
    	$this->title = $title;
    	$this->content = $content;
    	$this->img = $img;
    	$this->author = $author;
		$this->date = $date;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param string|null $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getContent(): ?string {
		return $this->content;
	}
	
	/**
	 * @param string|null $content 
	 * @return self
	 */
	public function setContent(?string $content): self {
		$this->content = $content;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getImg(): ?string {
		return $this->img;
	}
	
	/**
	 * @param string|null $img 
	 * @return self
	 */
	public function setImg(?string $img): self {
		$this->img = $img;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getAuthor(): ?string {
		return $this->author;
	}
	
	/**
	 * @param string|null $author 
	 * @return self
	 */
	public function setAuthor(?string $author): self {
		$this->author = $author;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
}