<?php

namespace App\Entities;



class Counter{
    private ?int $id;
    private ?int $number;

    public function __construct(?int $number, ?int $id=null){
        $this->id = $id;
    	$this->number = $number;
    }


	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return int|null
	 */
	public function getNumber(): ?int {
		return $this->number;
	}
	
	/**
	 * @param int|null $number 
	 * @return self
	 */
	public function setNumber(?int $number): self {
		$this->number = $number;
		return $this;
	}
}

