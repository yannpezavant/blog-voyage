<?php

namespace App\Repository;
use App\Entities\Article;
use App\Entities\Category;
use App\Entities\Comments;
use PDO;
use DateTime;


class ArticleRepository{

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }
    /**
     * Summary of sqlToArticle
     * @param array $line
     * @return Article
     */
    private function sqlToArticle(array $line):Article {
        $date = null;
        if(isset($line['date'])){
            $date = new DateTime($line['date']);
        }
        return new Article($line['title'], $line['content'], $line['img'], $line['author'], $date, $line['id']);
    }
    
    public function findAll(): array
    {
        /** @var Article[] */
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM article');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $articles[] = $this->sqlToArticle($item);
        }
        return $articles;
    }

     public function persist(Article $article) {
        $statement = $this->connection->prepare('INSERT INTO article (title, content, img, author, date) 
        VALUES (:title,:content,:img,:author,:date)');
        $statement->bindValue('title', $article->getTitle());
        $statement->bindValue('content', $article->getContent());
        $statement->bindValue('img', $article->getImg());
        $statement->bindValue('author', $article->getAuthor());
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->execute();

        $article->setId($this->connection->lastInsertId());
    }

    public function update(Article $article):void
    {
        $statement = $this->connection->prepare('UPDATE article 
        SET title=:title, content=:content, img=:img, author=:author, date=:date 
        WHERE id=:id');

        $statement->bindValue(':id', $article->getId(), PDO::PARAM_INT);
        $statement->bindValue('title', $article->getTitle());
        $statement->bindValue('content', $article->getContent());
        $statement->bindValue('img', $article->getImg());
        $statement->bindValue('author', $article->getAuthor());
        $statement->bindValue('date', $article->getDate()? $article->getDate()->format('Y-m-d'):null);

        $statement->execute();

    }
    public function findById(int $id):?Article {
        $statement = $this->connection->prepare('SELECT * FROM article WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToArticle($result);
        }
        return null;
    }

    
    public function delete(Article $article)
    { 
        $statement = $this->connection->prepare("DELETE FROM article  WHERE id =:id");
        $statement->bindValue("id", $article->getId(), PDO::PARAM_INT);
        $statement->execute();
        
    }

    public function findCommentsByArticle(Article $article):array{
        
        $array = [];
        $statement = $this->connection->prepare("SELECT *
        from comments
        left join article a
        on a.id=id_article
        where id_article=:id");

            $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);
            $statement->execute();

            $results = $statement->fetchAll();
            foreach($results as $line){
                $comments = new Comments($line["content"]);
                $array[] = $comments;
            }
            var_dump($results);
        return $array;
    }
}

