<?php

namespace App\Repository;
use App\Entities\Category;
use PDO;


class CategoryRepository{

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    private function sqlToCategory(array $line):Category {
        return new Category($line['name'], $line['id']);
    }
    public function findAll(): array
    {
        /** @var Category[] */
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM category');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $articles[] = $this->sqlToCategory($item);
        }
        return $articles;
    }

    public function persist(Category $category) {
        $statement = $this->connection->prepare('INSERT INTO category (name) 
        VALUES (:name)');
        $statement->bindValue('name', $category->getName());
        $statement->execute();

        $category->setId($this->connection->lastInsertId());
    }

    public function update(Category $category):void
    {
        $statement = $this->connection->prepare('UPDATE category 
        SET name=:name 
        WHERE id=:id');

        $statement->bindValue(':id', $category->getId(), PDO::PARAM_INT);
        $statement->bindValue(':name', $category->getName(), PDO::PARAM_STR);

        $statement->execute();
    }

    public function findById(int $id):?Category {
        $statement = $this->connection->prepare('SELECT * FROM category WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToCategory($result);
        }
        return null;
    }

    public function delete(Category $category)
    { 
        $statement = $this->connection->prepare("DELETE FROM category  WHERE id =:id");
        $statement->bindValue("id", $category->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    

}