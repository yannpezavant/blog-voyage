<?php

namespace App\Repository;
use App\Entities\Comments;
use App\Entities\Article;
use PDO;


class CommentsRepository{

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    private function sqlToComments(array $line):Comments {
        return new Comments($line['content'], $line['id']);
    }

    public function findAll(): array
    {
        /** @var Comments[] */
        $comments = [];
        $statement = $this->connection->prepare('SELECT * FROM comments');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $comments[] = $this->sqlToComments($item);
        }
        return $comments;
    }

    public function persist(Comments $comments) {
        $statement = $this->connection->prepare('INSERT INTO comments (content) 
        VALUES (:content)');
        $statement->bindValue('content', $comments->getContent());
        $statement->execute();

        $comments->setId($this->connection->lastInsertId());
    }

    public function update(Comments $comments):void
    {
        $statement = $this->connection->prepare('UPDATE comments 
        SET content=:content 
        WHERE id=:id');
        $statement->bindValue(':id', $comments->getId(), PDO::PARAM_INT);
        $statement->bindValue(':content', $comments->getContent(), PDO::PARAM_STR);
        $statement->execute();
    }

    public function findById(int $id):?Comments {
        $statement = $this->connection->prepare('SELECT * FROM comments WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToComments($result);
        }
        return null;
    }

    public function delete(Comments $comments)
    { 
        $statement = $this->connection->prepare("DELETE FROM comments  WHERE id =:id");
        $statement->bindValue("id", $comments->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    




}