<?php

namespace App\Repository;
use App\Entities\Counter;
use PDO;


class CounterRepository{

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    private function sqlToCounter(array $line):Counter {
        return new Counter($line['number'], $line['id']);
    }

    public function findAll(): array
    {
        /** @var Counter[] */
        $counter = [];
        $statement = $this->connection->prepare('SELECT * FROM counter');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $counter[] = $this->sqlToCounter($item);
        }
        return $counter;
    }

    public function persist(Counter $counter) {
        $statement = $this->connection->prepare('INSERT INTO counter (number) 
        VALUES (:number)');
        $statement->bindValue('number', $counter->getNumber());
        $statement->execute();

        $counter->setId($this->connection->lastInsertId());
    }

    public function update(Counter $counter):void
    {
        $statement = $this->connection->prepare('UPDATE counter 
        SET number=:number 
        WHERE id=:id');
        $statement->bindValue(':id', $counter->getId(), PDO::PARAM_INT);
        $statement->bindValue(':number', $counter->getNumber(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function findById(int $id):?Counter {
        $statement = $this->connection->prepare('SELECT * FROM counter WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToCounter($result);
        }
        return null;
    }

    public function delete(Counter $comments)
    { 
        $statement = $this->connection->prepare("DELETE FROM counter  WHERE id =:id");
        $statement->bindValue("id", $comments->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    



}